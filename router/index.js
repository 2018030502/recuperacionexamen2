// Importar el módulo body-parser y el framework Express
const bodyParser = require("body-parser");
const express = require("express");

// Crear una instancia de Router
const router = express.Router();
// Ruta principal que muestra una página con los datos del array "datos"
router.get("/", (req, res) => {
    res.render("entrada.html", {
    Header: "Compañia de Luz ",
    Footer: "Compañia de luz @Derechos Reservados Yamir Ezequiel Paez Ayala 2023",
    });
});
router.get('/entrada', (req, res) => {
    const datos = {
        nombre: req.query.nombre,
        TipoSer: req.query.TipoSer,
        consumidos: req.query.consumidos
    };
    res.render('resultados.html', datos );
  

});


router.post("/entrada", (req, res) => {
    const datos = {
        nombre: req.body.nombre,
        TipoSer: req.body.TipoSer,
        consumidos: req.body.consumidos
    };
    res.render("resultados.html", datos);


});
router.get("/resultados", (req, res) => {
    const datos = {
        nombre: req.query.nombre,
        TipoSer: req.query.TipoSer,
        consumidos: req.query.consumidos
    };
    res.render("resultados.html", datos);
  

    });
    router.post("/resultados", (req, res) => {
        const datos = {
        nombre: req.body.nombre,
        TipoSer: req.body.TipoSer,
       consumidos: req.body.consumidos
        };
        res.render("resultados.html", datos);
    });

// Exportar el módulo Router para que pueda ser utilizado en otros archivos
module.exports = router;